package kz.aitu.oop.practice.practice4.domain;

public abstract class Cost {
    private double cost;

    public Cost(double cost){
        setCost(cost);
    }

    public void setCost(double cost) {
        this.cost = cost;
    }


    public double getCost() {
        return cost;
    }
}