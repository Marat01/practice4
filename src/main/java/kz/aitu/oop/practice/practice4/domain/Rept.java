package kz.aitu.oop.practice.practice4.domain;

import kz.aitu.oop.practice.practice4.interfaces.Reptile;

public abstract class Rept extends Cost implements Reptile {
    public String name;


    public Rept(double cost,String name) {
        super(cost);
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
