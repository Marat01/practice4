package kz.aitu.oop.practice.practice4.domain;

import kz.aitu.oop.practice.practice4.interfaces.Testudines;

public class Tortoise extends Rept implements Testudines {

    public Tortoise(double cost, String name) {
        super(cost, name);
    }

    @Override
    public void swim() {
        Testudines();
    }

    @Override
    public void show() {
        swim();
    }

    @Override
    public void Testudines() {
        System.out.println("Type: tortoise. "
                + "Name: " + getName() + ". Cost: " + getCost() + "$. Class: Testudines.");
    }
}
