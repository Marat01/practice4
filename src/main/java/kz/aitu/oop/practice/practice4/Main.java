package kz.aitu.oop.practice.practice4;

import kz.aitu.oop.practice.practice4.domain.Aquarium;
import kz.aitu.oop.practice.practice4.domain.Crocodile;
import kz.aitu.oop.practice.practice4.domain.Snake;
import kz.aitu.oop.practice.practice4.domain.Tortoise;

public class Main {

    public static void main(String[] args) {
        Aquarium aq = new Aquarium();
        aq.addReptiles(new Crocodile(50000, "Alpha"));
        aq.addReptiles(new Snake(20000, "Mamba"));
        aq.addReptiles(new Tortoise(4500, "Flash"));

        aq.startShow();

        aq.deleteReptiles();
    }
}
