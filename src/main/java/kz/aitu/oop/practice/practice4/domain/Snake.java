package kz.aitu.oop.practice.practice4.domain;

import kz.aitu.oop.practice.practice4.interfaces.Lepidosauria;

public class Snake extends Rept implements Lepidosauria {

    public Snake(double cost, String name) {
        super(cost, name);
    }

    @Override
    public void Lepidosauria() {
        System.out.println("Type: snake. " + "Name: "
                + getName() + ". Cost: " + getCost() + "$. Class: Lepidosauria.");
    }

    @Override
    public void swim() {
        Lepidosauria();
    }

    @Override
    public void show() {
        swim();
    }
}
