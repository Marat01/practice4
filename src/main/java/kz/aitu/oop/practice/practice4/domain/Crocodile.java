package kz.aitu.oop.practice.practice4.domain;

import kz.aitu.oop.practice.practice4.interfaces.Archosauria;

public class Crocodile extends Rept implements Archosauria {

    public Crocodile(double cost, String name) {
        super(cost, name);
    }

    @Override
    public void Archosauria() {
        System.out.println("Type: crocodile. " + "Name: " + getName() +
                ". Cost: " + getCost() + "$. Class: Archosauria.");
    }

    @Override
    public void swim() {
        Archosauria();
    }

    @Override
    public void show() {
        swim();
    }
}
