package kz.aitu.oop.practice.practice4.domain;


import kz.aitu.oop.practice.practice4.interfaces.Reptile;

import java.util.LinkedList;
import java.util.List;

public class Aquarium {
    private List<Reptile> repltiles = new LinkedList<>();

    public void addReptiles(Reptile reptile){
        repltiles.add(reptile);
    }
    public void startShow(){
        repltiles.forEach(r->r.show());
    }

    public void deleteReptiles(){
        repltiles.clear();
    }

}
